# My SQL- SDET
SDET – MYSQL
Create the tables for User, Circle, User_Circle, Messages and User_Inbox.
  
Create table User(user_id integer(100),user_name varchar(100),password varchar(100));

Create table Circle(created_by varchar(100),created_on date);

Create table User_Circle(user_id varchar(100),subscriber varchar(100));

Create table Messages (user_name varchar(100),messages text,from_user_id varchar(100),to_user_id varchar(100));

Create table User_Inbox (user_id varchar(100),message text);

Insert the rows into the created tables (User, Circle, User_Circle, and Messages).

Insert into User(user_id,user_name,password) values(aa123,”aa”,”aa0123”),(bb123,”bb”,”bb@123”),(cc123,”cc”,”cc0123”),
(dd123,”dd”,”dd0123”),(ee123,”ee”,”ee0123”);

Insert into Circle(created_by, created_on) values(“rajni”,”2019-02-15”),(“kamal”,”2019-02-11”),(“vijay”,”2019-02-11”),(“ajith”,”2019-02-10”),(“Vikram”,”2019-02-20”);

Insert into User_Circle(user_id, subscriber) values(aa123,”facebook”),(bb123,”gmail”),(cc123,”yahoo”),(dd123,”gmail”),
(ee123,outlook);

Insert into Messages(user_name, messages, from_user_id, to_user_id) values (“Rajni”,”Hii”,aa123,dd123),(“kamal”,”Hello”,bb123,cc123),(“Vikram”,”Good Morning”,”ee123”,”aa123”);

 

Fetch the row from User table based on Id and Password.

Select user_id,password from user;

 

Fetch all the rows from Circle table based on the field created_by.

select created_by from circle;

 













Fetch all the Circles created after the particular Date.

select * from circle where created_on < "2019-02-15";

 

Fetch all the User Ids from UserCircle table subscribed to particular Circle.

Select user_id from user_circle where subscriber = "gmail";

 














Write Update query to unsubscribe to particular Circle for the given User Id.

Update user_circle SET subscriber=”unsubscribed” where user_id =11;

Select * from user_circle;

 

Fetch all the Messages from the Messages table sent by a particular User.

select messages from messages where from_user_id = 15;

 










Fetch all the Messages from the Messages table received by a particular User

select messages from messages where to_user_id=13;

 

Fetch all the Messages from the Messages table sent/received by a particular User. (both the Messages)

select messages from messages where from_user_id = 13 or to_user_id=13;

 











Write a query to send Message from particular User to another User(Use Message table - insert statement).


 

Write a query to delete particular Message which you received from User.

Delete from messages where from_user_id = 12;

select * from messages;

 


